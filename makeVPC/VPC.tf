
# Creating VPC, name should include the environment it's in?
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = var.tags
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.vpc.id
}
