resource "aws_subnet" "public1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.pub1_cidr
  map_public_ip_on_launch = true
  availability_zone = "${var.chosen_region}a"
  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "public2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.pub2_cidr
  map_public_ip_on_launch = true
  availability_zone = "${var.chosen_region}b"
  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "public3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.pub3_cidr
  map_public_ip_on_launch = true
  availability_zone = "${var.chosen_region}c"
  tags = {
    Name = var.tags
  }
}


resource "aws_subnet" "private1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.priv1_cidr
  availability_zone = "${var.chosen_region}a"
  map_public_ip_on_launch = false
  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "private2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.priv2_cidr
  availability_zone = "${var.chosen_region}b"
  map_public_ip_on_launch = false
  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "private3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.priv3_cidr
  availability_zone = "${var.chosen_region}c"
  map_public_ip_on_launch = false 
  tags = {
    Name = var.tags
  }
}
