resource "aws_route53_record" "jenkins_r53" {
  zone_id = var.dns_zone_id
  name    = "jenkins.${var.dns_domain}"
  type    = "A"
  ttl     = "60"
  records = [var.JenkinsIP]
}
