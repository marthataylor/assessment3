output "vpc_id" {
  value       = aws_vpc.vpc.id
  description = "ID of created VPC"
}

output "vpc_sg_id" {
  value       = aws_vpc.vpc.default_security_group_id
  description = "ID of automatically created VPC security group"
}

output "sg_id" {
  value       = aws_security_group.SG.id
  description = "ID of custom created security group"
}

output "vpc_default_route_table" {
  value       = aws_vpc.vpc.default_route_table_id
  description = "Default route table for VPC"
}

output "aws_internet_gateway" {
  value       = aws_internet_gateway.default.id
  description = "Gateway ID"
}

output "subnet_id_pub1" {
  value       = aws_subnet.public1.id
  description = "ID of created public subnet"
}

output "subnet_id_pub2" {
  value       = aws_subnet.public2.id
  description = "ID of created public subnet"
}

output "subnet_id_pub3" {
  value       = aws_subnet.public3.id
  description = "ID of created public subnet"
}

output "subnet_id_priv1" {
  value       = aws_subnet.private1.id
  description = "ID of created private subnet"
}

output "subnet_id_priv2" {
  value       = aws_subnet.private2.id
  description = "ID of created private subnet"
}

output "subnet_id_priv3" {
  value       = aws_subnet.private3.id
  description = "ID of created private subnet"
}

output "homeIP" {
  value       = var.homeIP
  description = "home IPs"
}
