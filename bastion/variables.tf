# Define all variables
variable "vpc_cidr" {
    type    = string
    description = "CIDR for the VPC"
}

variable "chosen_region" {
    type    = string
    description = "Chosen region to create RDS in"
}

variable "bucket_name" {
    type    = string
    description = "Name of S3 bucket"
}

variable "tags" {
    type    = string
    description = "Tag chosen in tfvars file"
}

variable "db_name" {
    type    = string
    description = "Name of database"
}

variable "db_user" {
    type    = string
    description = "Name of database user"
}

variable "db_password" {
    type    = string
    description = "Database password"
}

variable "homeIP" {
    type    = list(string)
    description = "Home IPs"
}

variable "dns_domain" {
    description = "Domain of the bastion"
}

variable "amiID" {
    type = string
    description = "Chosen AMI ID"
}

variable "ssh_key_name" {
    type    = string
}

variable "dns_zone_id" {
    type    = string
}

variable "JenkinsIP" {
    type    = string
}
