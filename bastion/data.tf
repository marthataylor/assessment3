
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/makevpc.tfstate"
    region = var.chosen_region
  }
}

#data "terraform_remote_state" "rds" {
  #backend = "s3"
  #config = {
  #  bucket = var.bucket_name
  #  key    = "global/s3/statefiles/rds.tfstate"
  #  region = var.chosen_region
  #}
#}

data "terraform_remote_state" "tfstart" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/tfstart.tfstate"
    region = var.chosen_region
  }
}
