
# Create a new load balancer for the bastion
resource "aws_elb" "bastion" {
  name               = "bastion-terraform-elb"
  subnets = [data.terraform_remote_state.vpc.outputs.subnet_id_pub1, data.terraform_remote_state.vpc.outputs.subnet_id_pub2, data.terraform_remote_state.vpc.outputs.subnet_id_pub3]
  security_groups = [aws_security_group.LBbastion.id]
  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "bastion-terraform-elb"
  }
}

# Create configuration for bastion
resource "aws_launch_configuration" "bastion_lc" {
    name = "bastion-lc-${var.tags}"
    image_id = var.amiID

    instance_type = "t2.micro"

    iam_instance_profile = data.terraform_remote_state.tfstart.outputs.iam_role_name
    key_name = var.ssh_key_name
    security_groups = [aws_security_group.bastion.id]

    user_data = file("populate")

    lifecycle {
      create_before_destroy = true
    }


}

# Create autoscaling group for bastion and load balancer
resource "aws_autoscaling_group" "bastion_asg" {
  name                      = "bastion_asg"
  max_size                  = 1
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  #placement_group           = aws_placement_group.bastion.id
  launch_configuration      = aws_launch_configuration.bastion_lc.name
  vpc_zone_identifier       = [data.terraform_remote_state.vpc.outputs.subnet_id_pub1, data.terraform_remote_state.vpc.outputs.subnet_id_pub2, data.terraform_remote_state.vpc.outputs.subnet_id_pub3]

  timeouts {
    delete = "15m"
  }

}

# Attaching the autoscaling group to the load balancer
resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  depends_on = [aws_autoscaling_group.bastion_asg, aws_elb.bastion]
  autoscaling_group_name = aws_autoscaling_group.bastion_asg.id
  elb                    = aws_elb.bastion.id
}


##############################################################################



# Creating security group for bastion
resource "aws_security_group" "bastion" {
    name = "SG_bastion"
    description = "Allow access from outside the VPC to the DB private subnet"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.vpc_cidr]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id

    tags = {
        Name = "${var.tags}_BastionSG"
    }
}

# Creating security group for bastion load balancer
resource "aws_security_group" "LBbastion" {
    name = "SG_LBbastion"
    description = "Allow access from outside the VPC to the DB private subnet"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = concat(var.homeIP, [var.JenkinsIP])
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id

    tags = {
        Name = "${var.tags}_LBbastionSG"
    }
}
# Create a DNS name for the bastion
resource "aws_route53_record" "bastion" {
  zone_id = var.dns_zone_id
  name = "bastion.${var.dns_domain}"
  type = "CNAME"
  ttl = "30"

  records = [aws_elb.bastion.dns_name]
}
