#!/bin/bash

ENV=$1

if [[ -z $ENV ]]
then
  echo "Not given or invalid parameter ENVIRONMENT (must be dev or prod)"
  exit 1
fi

# Get region and bucket name from tfvars
chosen_region=$(cat ../tfstart/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ../tfstart/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
export chosen_region bucket_name

# Download file containing Jenkins IP from s3 bucket
aws s3 cp s3://$bucket_name/global/s3/IPs/ipforjenkins ipforjenkins
echo '/32' >> ipforjenkins
jenkinsip=$(cat ipforjenkins)
rm ipforjenkins
cat >>../tfstart/$ENV.tfvars <<_END_
# Place the IP into a variable to use in this directory
JenkinsIP="$jenkinsip"
_END_


# Run the terraform in this directory
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region"
terraform apply -auto-approve -var-file=../tfstart/$ENV.tfvars
git checkout ../tfstart/$ENV.tfvars
