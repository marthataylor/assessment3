#!/bin/bash

#Wrapper script for building network infrastructure with Terraform

ENV=$1

if [[ -z $ENV ]]
then
  echo "Not given or invalid parameter ENVIRONMENT (dev or prod)"
  exit 1
fi

chosen_region=$(cat ../tfstart/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ../tfstart/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')

export chosen_region bucket_name


terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=global/s3/statefiles/bastion.tfstate"
terraform destroy -auto-approve -var-file="../tfstart/${ENV}.tfvars"
