#!/bin/bash

# Check for environment as argument
ENV=$1

if [[ -z $ENV ]]
then
  echo "Not given or invalid parameter ENVIRONMENT (must be dev or prod)"
  exit 1
fi

# Get region, bucket name, and profile from tfvars
chosen_region=$(cat ../tfstart/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ../tfstart/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
profile=$(cat ../tfstart/${ENV}.tfvars | grep 'profile' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_name=$(cat ../tfstart/${ENV}.tfvars | grep 'ssh_key_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
export chosen_region bucket_name profile ssh_key_name

# Copy the users private key from the S3 bucket
aws s3 cp s3://$bucket_name/global/s3/keys/$ssh_key_name /var/lib/jenkins/$ssh_key_name #--profile $profile
private_key=$(cat /var/lib/jenkins/$ssh_key_name)
rm /var/lib/jenkins/$ssh_key_name

# Run the terraform in this directory

terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region"
terraform apply -auto-approve -var-file="../tfstart/${ENV}.tfvars" -var "private_key=$private_key"
