
# Get data from the remote states of makevPC and bastion directories
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/makevpc.tfstate"
    region = var.chosen_region
  }
}

data "terraform_remote_state" "bastion" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/bastion.tfstate"
    region = var.chosen_region
  }
}
