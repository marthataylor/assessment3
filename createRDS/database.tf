# Make RDS database

# Create security group for database
resource "aws_security_group" "db" {
  name = "vpc_db"
  description = "Allow traffic on VPC subnets to access DB"

  ingress {
    from_port = 0
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
      from_port = -1
      to_port = -1
      protocol = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id

  tags = {
      Name = "${var.tags}_DBSG"
  }
}

# Assing private subnets of VPC to the database subnet
resource "aws_db_subnet_group" "default" {
  name = "${lower(var.tags)}dbsg"
  subnet_ids = [data.terraform_remote_state.vpc.outputs.subnet_id_priv1, data.terraform_remote_state.vpc.outputs.subnet_id_priv2 ,data.terraform_remote_state.vpc.outputs.subnet_id_priv3]
}

# Create database instance
resource "aws_db_instance" "default" {
  allocated_storage = 10
  storage_type = "gp2"
  multi_az = false
  engine = "mysql"
  engine_version = "5.7.16"
  instance_class = "db.m3.medium"
  name = var.db_name
  username = var.db_user
  password = var.db_password
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.db.id]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  identifier = "${lower(var.tags)}db"

  tags = {
      Name = "${var.tags}_DB"
  }
}

# Use the populate.tpl file to populate the RDS
data "template_file" "rds_pc" {
  template = file("populate.tpl")

  vars  = {
    dbserver = aws_db_instance.default.endpoint
    dbuser = var.db_user
    dbpassword = var.db_password
    dbname = var.db_name
  }
}
resource "null_resource" "debug" {
  provisioner "local-exec" {
    command = "echo ${data.terraform_remote_state.bastion.outputs.bastionDNS}"
  }
}


# Upload populate script to bastion
resource "null_resource" "uploadcreatescript" {
  depends_on = [aws_db_instance.default]
  provisioner "file" {
    content     = data.template_file.rds_pc.rendered
    destination = "dbBuild"

    connection {
      type     = "ssh"
      user     = "ec2-user"
      private_key = var.private_key
      # IP or DNS of bastion
      host     = data.terraform_remote_state.bastion.outputs.bastionDNS
    }
  }
}

# Execute populate script 
resource "null_resource" "executescript" {
  depends_on = [null_resource.uploadcreatescript]
  provisioner "remote-exec" {
    inline = [ "/bin/chmod 755 dbBuild", "./dbBuild" ]

    connection {
      type     = "ssh"
      user     = "ec2-user"
      private_key = var.private_key
      # IP or DNS of bastion
      host     = data.terraform_remote_state.bastion.outputs.bastionDNS
    }
  }
}
