# Declare all variables

variable "vpc_cidr" {
    type    = string
    description = "CIDR for the VPC"
}

variable "chosen_region" {
    type    = string
    description = "Chosen region to create RDS in"
}

variable "bucket_name" {
    type    = string
    description = "Name of S3 bucket"
}

variable "tags" {
    type    = string
    description = "Tag chosen in tfvars file"
}

variable "db_name" {
    type    = string
    description = "Name of database"
}

variable "db_user" {
    type    = string
    description = "Name of database user"
}

variable "db_password" {
    type    = string
    description = "Database password"
}

variable "env" {
    type    = string
    description = "Chosen environment"
}

variable "ssh_key_path" {
  type    = string
}

variable "private_key" {
  type    = string
}
