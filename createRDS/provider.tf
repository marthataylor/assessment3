# Contains amazon provider info

provider "aws" {
  region = var.chosen_region
}
