#!/bin/bash -xv

mkdir /opt/petclinic/config
cat > /opt/petclinic/config/application.properties << _END
database=mysql
spring.datasource.url=jdbc:mysql://${dbip}/${dbname}
spring.datasource.username=${dbuser}
spring.datasource.password=${dbpasswd}
spring.jpa.hibernate.ddl-auto=none
logging.level.org.springframework=INFO
spring.profiles.active=production
_END

#Start PetClinic
cd /opt/petclinic
nohup java -jar spring-petclinic-2.0.0.jar &
