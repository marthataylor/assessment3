output "app_pub_dns" {
  value = aws_instance.petcliniclaunch.public_dns
}

output "app_iid" {
  value = aws_instance.petcliniclaunch.id
}

#output "pc_sgid" {
#  value = data.terraform_remote_state.infra0.outputs.pc_sgid
#}
