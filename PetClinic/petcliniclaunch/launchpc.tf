data "template_file" "user-data" {
  template = file("${path.module}/pclaunch-userdata.tpl")
  vars = {
    dbip     = trimsuffix(data.terraform_remote_state.db.outputs.dbEndpoint, ":3306")
    dbuser   = var.db_user
    dbpasswd = var.db_password
    dbname   = var.db_name
  }
}

resource "aws_instance" "petcliniclaunch" {
  ami                    = data.terraform_remote_state.infra.outputs.pc_ami
  instance_type          = var.instance_type
  key_name               = var.ssh_key_name
  iam_instance_profile   = data.terraform_remote_state.tfstart.outputs.iam_role_name
  vpc_security_group_ids = [data.terraform_remote_state.infra0.outputs.sg_id] #[var.sgid]
  subnet_id              = data.terraform_remote_state.infra0.outputs.subnet_id_pub1
  user_data              = data.template_file.user-data.rendered

  tags = {
    Name = "${var.jobtag}-${var.tags}-${var.env}"
  }
}

resource "null_resource" "check_launch" {
  depends_on = [aws_instance.petcliniclaunch]
  provisioner "local-exec" {
    command = "while [[ -z $(curl -s ${aws_instance.petcliniclaunch.public_dns}:8080) ]];do echo 'Instance is Not Yet Initialized'; sleep 20; done; echo 'curl success: '$(curl -s ${aws_instance.petcliniclaunch.public_dns}:8080 | wc -l)"
  }
}
