#!/bin/bash -xv

sudo yum -y update
sudo yum -y install java-1.8.0 java-1.8.0-devel wget git
cd /tmp
git clone https://mollymcc@bitbucket.org/JangleFett/petclinic.git
sudo wget https://www-us.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
cd /opt
mkdir petclinic
sudo tar xzvf /tmp/apache-maven-3.6.3-bin.tar.gz
cd /tmp/petclinic
/opt/apache-maven-3.6.3/bin/mvn -Dmaven.test.skip=true package
mv target/spring-petclinic-2.0.0.jar /opt/petclinic
rm -rf /tmp/petclinic /tmp/*.tar.gz
#copy jar file to S3 bucket
cd /opt/petclinic
sudo aws s3 cp spring-petclinic-2.0.0.jar s3://skm-assignment2/jarfile --region us-west-2

mkdir /opt/petclinic/config
cat > /opt/petclinic/config/application.properties << _END
database=mysql
spring.datasource.url=jdbc:mysql:///petclinic
spring.datasource.username=
spring.datasource.password=
spring.jpa.hibernate.ddl-auto=none
logging.level.org.springframework=INFO
spring.profiles.active=production
_END

cat > /etc/init.d/pc << _END
#!/bin/bash
#description: control petclinic
#chkconfig: 2345 99 99
case \$1 in
  start)
    cd /opt/petclinic
    nohup java -jar spring-petclinic-2.0.0.jar &
    ;;
  stop)
    kill \$(ps -ef | grep petclinic | grep -v grep | awk '{print \$2}')
    ;;
  status)
    ps -ef | grep petclinic | grep -v grep | awk '{print \$2}'
    ;;
esac
_END

chmod +x /etc/init.d/pc
chkconfig --add pc
sudo service pc start

