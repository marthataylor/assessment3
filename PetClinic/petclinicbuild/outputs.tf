output "pc_pub_dns" {
  value = aws_instance.petclinicbuild.public_dns
}

output "pc_pub_id" {
  value = aws_instance.petclinicbuild.id
}
