#!/bin/bash -xv
sudo yum -y update
sudo yum -y install java-1.8.0 java-1.8.0-devel wget git
cd /tmp
git clone https://mollymcc@bitbucket.org/JangleFett/petclinic.git
sudo wget https://www-us.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
cd /opt
mkdir petclinic
sudo tar xzvf /tmp/apache-maven-3.6.3-bin.tar.gz
cd /tmp/petclinic
/opt/apache-maven-3.6.3/bin/mvn -Dmaven.test.skip=true package

sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.6.3/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.6.3/bin/mvn

mv target/spring-petclinic-2.0.0.jar /opt/petclinic
rm -rf /tmp/petclinic /tmp/*.tar.gz
cd /opt/petclinic
sudo aws s3 cp spring-petclinic-2.0.0.jar s3://${bucket_name}/global/s3/jarfiles/jarfile --region ${chosen_region}

mkdir /opt/petclinic/config
cat > /opt/petclinic/config/application.properties << _END
database=mysql
spring.datasource.url=jdbc:mysql://${dbip}/${dbname}
spring.datasource.username=${dbuser}
spring.datasource.password=${dbpasswd}
spring.jpa.hibernate.ddl-auto=none
logging.level.org.springframework=INFO
spring.profiles.active=production
_END

#Start PetClinic
cd /opt/petclinic
nohup java -jar spring-petclinic-2.0.0.jar &
