data "terraform_remote_state" "infra0" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/makevpc.tfstate"
    region = var.chosen_region
  }
}

#For RDS database info - dbip, dbuser
data "terraform_remote_state" "db" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/rds.tfstate"
    region = var.chosen_region
  }
}

data "terraform_remote_state" "tfstart" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/tfstart.tfstate"
    region = var.chosen_region
  }
}
