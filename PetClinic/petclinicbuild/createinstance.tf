data "template_file" "user-data" {
  template = file("${path.module}/pcami-userdata.tpl")
  vars = {
    dbip          = trimsuffix(data.terraform_remote_state.db.outputs.dbEndpoint, ":3306") #this_db_instance_endpoint
    dbuser        = var.db_user                                                            #data.terraform_remote_state.db.outputs #this_db_instance_username
    dbpasswd      = var.db_password                                                        #get from vault
    chosen_region = var.chosen_region
    dbname        = var.db_name
    bucket_name   = var.bucket_name
  }
}

resource "null_resource" "debug" {
  provisioner "local-exec" {
    command = "echo ${var.db_name} ${var.db_user} ${var.db_password} ${trimsuffix(data.terraform_remote_state.db.outputs.dbEndpoint, ":3306")}"
  }
}

resource "aws_instance" "petclinicbuild" {
  ami                    = var.amiID
  instance_type          = var.instance_type
  key_name               = var.ssh_key_name
  iam_instance_profile   = data.terraform_remote_state.tfstart.outputs.iam_role_name #var.iamprofile
  vpc_security_group_ids = [data.terraform_remote_state.infra0.outputs.sg_id]
  subnet_id              = data.terraform_remote_state.infra0.outputs.subnet_id_pub1
  user_data              = data.template_file.user-data.rendered

  tags = {
    Name = "${var.jobtag}-${var.tags}-${var.env}"
  }
}

resource "null_resource" "check_instancebuild" {
  depends_on = [aws_instance.petclinicbuild]
  provisioner "local-exec" {
    command = "while [[ -z $(curl -s ${aws_instance.petclinicbuild.public_dns}:8080) ]];do echo 'not yet'; sleep 120; done; echo 'curl success: '$(curl -s ${aws_instance.petclinicbuild.public_dns}:8080 | wc -l)"
  }
}
