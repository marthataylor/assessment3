data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/appbuild.tfstate"
    region = var.chosen_region
  }
}

data "terraform_remote_state" "infra0" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "global/s3/statefiles/makevpc.tfstate"
    region = var.chosen_region
  }
}
