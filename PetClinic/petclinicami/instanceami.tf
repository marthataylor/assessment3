resource "aws_ami_from_instance" "petclinic-ami" {
  name               = "petclinic-ami-${var.AMItimestamp}"
  source_instance_id = data.terraform_remote_state.infra.outputs.pc_pub_id

  tags = {
    Name = "${var.jobtag}-${var.tags}-${var.env}-${var.AMItimestamp}"
  }
}
