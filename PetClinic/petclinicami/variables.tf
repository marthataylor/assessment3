variable "jobtag" {
  type    = string
  default = "PetClinicAmi"
}

variable "AMItimestamp" {
  type = string
}

#From dev.tfvars are the variables:
variable "bucket_name" {
  type = string
}

variable "chosen_region" {
  type = string
}

variable "tags" {
  type = string
}

variable "env" {
  type = string
}

#chosen_region="us-east-1"
#profile="academy"
#ssh_key_path="~/Desktop/SzymonVirginia.pem"
#ssh_key_name="SzymonVir2"
#bucket_name="smm-assessment3"
#amiID="ami-0947d2ba12ee1ff75"
#subnetID="subnet-762dd410"
#tags="MSMassessment3"
#env="dev"
