resource "aws_route53_record" "petclinic_r53" {
  zone_id = var.dns_zone_id
  name    = "petclinic.${var.dns_domain}"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elb.petclinic_lb.dns_name]
}
