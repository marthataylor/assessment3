data "aws_ami" "petclinic_ami" {
  most_recent      = true
  owners           = ["self"]

  filter {
    name   = "name"
    values = ["petclinic*"]
  }
}

data "template_file" "petclinic_template" {
  template = file("../PetClinic/petcliniclaunch/pclaunch-userdata.tpl")
  vars = {
    dbip = trimsuffix(data.terraform_remote_state.db.outputs.dbEndpoint, ":3306")
    dbuser = var.db_user
    dbpasswd = var.db_password
    dbname = var.db_name
  }
}


resource "aws_launch_configuration" "petclinic_lc" {
  name_prefix   = "${var.project_name}-${var.env}-petclinic-LC"
  image_id      = data.aws_ami.petclinic_ami.id
  instance_type = var.instance_type
  key_name = var.ssh_key_name
  security_groups = [data.terraform_remote_state.infra0.outputs.sg_id]
  user_data = data.template_file.petclinic_template.rendered

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "petclinic_asg" {
  name                      = "${var.project_name}-${var.env}-petclinic-ASG"
  max_size                  = 3
  min_size                  = 1
  desired_capacity          = 2
  health_check_type         = "ELB"
  launch_configuration      = aws_launch_configuration.petclinic_lc.name
  vpc_zone_identifier       = [data.terraform_remote_state.infra0.outputs.subnet_id_pub1,
    data.terraform_remote_state.infra0.outputs.subnet_id_pub2, data.terraform_remote_state.infra0.outputs.subnet_id_pub3]

  timeouts {
    delete = "15m"
  }

}

resource "aws_autoscaling_attachment" "asg_attachment_petclinic" {
  autoscaling_group_name = aws_autoscaling_group.petclinic_asg.id
  elb                    = aws_elb.petclinic_lb.id
}
