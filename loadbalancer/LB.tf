# Create a load balancer
resource "aws_elb" "petclinic_lb" {
  name               = "${var.project_name}-${var.env}-pc-LB"
  subnets = [data.terraform_remote_state.infra0.outputs.subnet_id_pub1, data.terraform_remote_state.infra0.outputs.subnet_id_pub2, data.terraform_remote_state.infra0.outputs.subnet_id_pub3]
  security_groups = [data.terraform_remote_state.infra0.outputs.sg_id]
  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 8080
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "http:8080/"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = var.tags
  }
}
