variable "chosen_region" {
  type    = string
}

variable "env" {
  type    = string
}

variable "project_name" {
  type    = string
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "bucket_name" {
  type = string
}

variable "tags" {
  type    = string
}

variable "ssh_key_name" {
  type    = string
}

variable "dns_domain" {
  type    = string
}

variable "dns_zone_id" {
  type    = string
}

variable "db_user" {
  type    = string
}

variable "db_password" {
  type    = string
}

variable "db_name" {
  type    = string
}
