data "template_file" "user-data" {
  template = file("${path.module}/pcami-userdata.tpl")
  vars = {
    dbip = data.terraform_remote_state.db.outputs.dbEndpoint #this_db_instance_endpoint
    dbuser = var.db_user #data.terraform_remote_state.db.outputs #this_db_instance_username
    dbpasswd = var.db_password #get from vault
  }
}

resource "aws_instance" "vaultInstance" {
  ami           = var.amiID
  instance_type = var.instance_type
  key_name      = var.ssh_key_name
  iam_instance_profile = data.terraform_remote_state.tfstart.outputs.iam_role_name #var.iamprofile
  vpc_security_group_ids = [data.terraform_remote_state.infra0.outputs.sg_id]
  subnet_id              = data.terraform_remote_state.infra0.outputs.subnet_id_pub1
  user_data = data.template_file.user-data.rendered

  tags = {
    Name = "${var.jobtag}-${var.tags}-${var.env}"
  }
}

resource "null_resource" "check_instancebuild" {
  depends_on = [aws_instance.petclinicbuild]
  provisioner "local-exec" {
    command = "echo 'curl success: '$(curl -s ${aws_instance.petclinicbuild.public_dns} | wc -l)"
  }
}
}
