data "tls_public_key" "key_pub" {
  private_key_pem = var.private_key
}

resource "aws_key_pair" "key" {
  key_name   = var.ssh_key_name
  public_key = data.tls_public_key.key_pub.public_key_openssh
}
