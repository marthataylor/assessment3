#General Settings
chosen_region="us-east-1"
profile="academy"

#Security Settings
ssh_key_path="~/Desktop/SzymonVirginia.pem"
ssh_key_name="Szymon_Vir"
bucket_name="smmassessment3"

#Instance Settings
amiID="ami-0947d2ba12ee1ff75"
subnetID="subnet-762dd410"

#Tagging Settings
tags="MSMassessment"
env="prod"
project_name="AcademyAssessmentMSM"

#Network Settings
homeIP=["81.108.146.109/32", "109.148.23.91/32", "86.10.108.213/32"]
vpc_cidr="172.31.0.0/16"
pub1_cidr="172.31.0.0/19"
pub2_cidr="172.31.32.0/19"
pub3_cidr="172.31.64.0/19"
priv1_cidr="172.31.96.0/19"
priv2_cidr="172.31.128.0/19"
priv3_cidr="172.31.160.0/19"
db_name="petclinic"
db_user="petclinic"
db_password="petclinic"
dns_domain="SMM.academy.grads.al-labs.co.uk"
dns_zone_id="Z07626429N74Z31VDFLI"
