resource "aws_iam_role" "super_role" {
  name = "${var.project_name}-${var.env}-super-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name = var.tags
  }
}

resource "aws_iam_policy" "super_policy" {
  name        = "${var.project_name}-${var.env}-jenkins-policy"
  description = "Policy for assessment3"

  policy = <<EOF
{ "Version": "2012-10-17", "Statement": [ { "Action": [ "rds:*", "ec2:DescribeAccountAttributes", "ec2:DescribeAvailabilityZones", "ec2:DescribeInternetGateways", "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVpcAttribute", "ec2:DescribeVpcs", "sns:ListSubscriptions", "sns:ListTopics", "sns:Publish", "logs:DescribeLogStreams", "logs:GetLogEvents", "outposts:GetOutpostInstanceTypes" ], "Effect": "Allow", "Resource": "*" }, { "Action": "iam:CreateServiceLinkedRole", "Effect": "Allow", "Resource": "*", "Condition": { "StringLike": { "iam:AWSServiceName": [ "rds.amazonaws.com", "rds.application-autoscaling.amazonaws.com" ] } } }, { "Action": "ec2:*", "Effect": "Allow", "Resource": "*" }, { "Effect": "Allow", "Action": "elasticloadbalancing:*", "Resource": "*" }, { "Effect": "Allow", "Action": "cloudwatch:*", "Resource": "*" }, { "Effect": "Allow", "Action": "autoscaling:*", "Resource": "*" }, { "Effect": "Allow", "Action": "iam:CreateServiceLinkedRole", "Resource": "*", "Condition": { "StringEquals": { "iam:AWSServiceName": [ "autoscaling.amazonaws.com", "ec2scheduled.amazonaws.com", "elasticloadbalancing.amazonaws.com", "spot.amazonaws.com", "spotfleet.amazonaws.com", "transitgateway.amazonaws.com" ] } } }, { "Effect": "Allow", "Action": [ "iam:*", "organizations:DescribeAccount" ], "Resource": "*" }, { "Effect": "Allow", "Action": "s3:*", "Resource": "*" }, { "Effect": "Allow", "Action": [ "route53:CreateHostedZone", "route53domains:*" ], "Resource": [ "*" ] }, { "Effect": "Allow", "Action": [ "route53:*", "route53domains:*", "cloudfront:ListDistributions", "elasticloadbalancing:DescribeLoadBalancers", "elasticbeanstalk:DescribeEnvironments", "s3:ListBucket", "s3:GetBucketLocation", "s3:GetBucketWebsite", "ec2:DescribeVpcs", "ec2:DescribeVpcEndpoints", "ec2:DescribeRegions", "sns:ListTopics", "sns:ListSubscriptionsByTopic", "cloudwatch:DescribeAlarms", "cloudwatch:GetMetricStatistics" ], "Resource": "*" } ] }
EOF

}

resource "aws_iam_role_policy_attachment" "super_policy_attach" {
  role       = aws_iam_role.super_role.name
  policy_arn = aws_iam_policy.super_policy.arn

}

resource "aws_iam_instance_profile" "super_profile" {
  name = "${var.project_name}-${var.env}-super-role"
  role = aws_iam_role.super_role.name

}
