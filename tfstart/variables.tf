
variable "chosen_region" {
  type    = string
}

variable "profile" {
  type    = string
}

variable "homeIP" {
  type    = list(string)
}

variable "ssh_key_name" {
  type    = string
}


variable "private_key" {
  type    = string
}

variable "bucket_name" {
  type = string
}

variable "amiID" {
  type = string
}

variable "subnetID" {
  type = string
}

variable "env" {
  type    = string
}

variable "tags" {
  type    = string
}

variable "project_name" {
  type    = string
}
