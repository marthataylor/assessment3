output "iam_role_name" {
  value       = aws_iam_instance_profile.super_profile.name
  description = "Name of IAM role"
}

output "sg_name" {
  value       = aws_security_group.SG.name
  description = "Name of Jenkins SG"
}
