all: infra srv

init: initinfra initsrv

initinfra:
	cd tfstart && terraform init

initsrv:
	cd makeVPC && terraform init

infra:
	@echo "Building/Updating infrastructure"
	cd makeVPC && terraform apply -auto-approve -var-file=$(ENV).tfvars

srv:
	@echo "Building/Updating the web server"
	cd tfstart && terraform apply -auto-approve -var-file=$(ENV).tfvars

delete:	rmsrv rminfra

rmsrv:
	@echo "Deleting the web server"
	cd tfstart && terraform destroy -auto-approve -var-file=$(ENV).tfvars

rminfra:
	@echo "Deleting the infrastructure"
	cd makeVPC && terraform destroy -auto-approve -var-file=$(ENV).tfvars

plan: planinfra plansrv

planinfra:
	@echo  "Planing the infrastructure"
	cd makeVPC && terraform plan -var-file=$(ENV).tfvars
	cd ..

plansrv:
	@echo "Planning the web server"
	cd tfstart && terraform plan -var-file=$(ENV).tfvars

show: showinfra showsrv

showinfra:
	cd makeVPC && terraform show

showsrv:
	cd tfstart && terraform show
