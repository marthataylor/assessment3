# ReadMe for Assessment 3  
#### Szymon, Molly, and Martha

This ReadMe file contains documentation that will assist with the use of our code.

## Contents:
 - Pre-requisites
 - Initial variable alterations
 - Steps of using the code
 - Re-building the structure
 - Error Support

## Pre-requisites

###### Python3
* Make sure you have the current version of Python3 installed on your machine.
* Install PIP
* Install packages from requirements.txt

###### AWS
* Downloaded credentials file

###### Bitbucket Webhook
A web-hook to build the Jenkins jobs has been partly created for this BitBucket repository, however in order for it to work successfully there are some extra steps complete. If set up, any push or merged pull request to the repository will trigger the relevant jobs to be built in Jenkins.

- Install the Jenkins plugin:
    - Bitbucket
- Adapt the .xml config files for the Jenkins jobs to select the build trigger 'Build when a change is pushed to Bitbucket'.
- The webhook should be set up in the BitBucket repository settings to link to the Jenkins server on
    - http://SMM.academy.grads.al-labs.co.uk:8080/bitbucket-hook/
- On any push or merged pull request, the job with the build trigger set as described will be built. By setting appropriate Post Build Actions to build other projects, the webhook could be set to build certain jobs in turn, each triggering the next stage of the build.

## Initial variable alterations
### Altering dev.tfvars and prod.tfvars files
You will need to alter certain variables inside the dev.tfvars and prod.tfvars files, which can be found in the tfstart directory.

* *dev.tfvars* will be used for setting variables in DEVELOPMENT environment
* *prod.tfvars* will be used for setting variables in PRODUCTION environment

Change the following variables:

* __chosen_region__ = AWS region you wish to use
* __profile__ = AWS profile you wish to use
* __ssh_key_path__ = path to you private key
* __ssh_key_name__= name of your key on aws
* __bucket_name__= name you wish to call the S3 bucket
* __tags__= tag name for your infrastructure pieces
* __project_name__= name of your project
* __homeIP__= your home ip
* __db_user__= database username
* __db_password__= database password
* __dns_domain__= desired domain name
* __dns_zone_id__= id of the public hosted zone

The following variables do not need to be altered, but can be if you so wish:

- __amiID__= change this to be correct for the region you are operating in
- __subnetID__= change this to be correct for the region you are operating in
- __vpc_cidr__= Desired cidr for the VPC created by jenkins
- __pub1_cidr__= Desired cidr for public subnet 1
- __pub2_cidr__= Desired cidr for public subnet 2
- __pub3_cidr__= Desired cidr for public subnet 3
- __priv1_cidr__= Desired cidr for private subnet 1
- __priv2_cidr__= Desired cidr for private subnet 2
- __priv3_cidr__= Desired cidr for private subnet 3

Do not alter this variable:

* __db_name__= petclinic

## Steps ##

### Initial script
Creation of the infrastructure can be done with one command. Run the start script with the environment that you are using as an argument i.e. dev
`./start dev `

You must be within an environment with ansible installed in order to run this script, as an ansible playbook command is ran in this script.

This script will use terraform to create an S3 bucket, SSH key, jenkins IAM role and Jenkins security group. It will then use ansible to create a server with git, java, jenkins, terraform, python and ansible installed. This server will be the controller for the infrastructure that is created later.

Note that the pem file containing your private key will be uploaded to the S3 bucket.

The Jenkins jobs in following order should then be run from the Jenkins server:

* (dev/prod)makevpcconfig
* (dev/prod)bastionconfig
* (dev/prod)createRDSconfig
* builder_petclinicconfig(dev/prod)
* petclinic_AMIconfig(dev/prod)
* destroy_petclinicconfig(dev/prod)
* (dev/prod)buildlbconfig

`Jenkins username: admin password: admin`

### Jenkins Jobs: ###

##### Make VPC #####

The network infrastructure is built with Terraform. It includes the following elements:

*  New VPC for dev/prod environments.
*  Set of 3 public and 3 private subnets in 3 different availability zones.
* Internet Gateway that is attached to the VPC.
* Route table for the 3 public subnets.
* Security group which is used by PetClinic instances and the application loadbalancer.
* Route53 record for the Jenkins instance

##### Create bastion and loadbalancer #####

The bastion and its loadbalancer are created in the bastion.tf directory. 

- Load balancer and bastion are created.
- LB and bastion have their own security groups. Bastion is open on port 22 only to resources within the VPC created in the makeVPC job. Load balancer is open on port 22 to Jenkins and user home IP.
- Auto scaling group created and attached to bastion.
- Bastion template script installs and starts mysql mariadb on bastion instance. 
- DNS name is created for the bastion.

##### Create RDS database #####

In the createRDS.tf directory, the RDS is created.

- Security group created for database allowing inbound access on port 3306.
- Database is created with user variables specified in dev/prod.tfvars
- Script to create schema and insert data into database is uploaded to the bastion and executed.

##### Application Initial Build #####

Terraform is used to build the Pet Clinic Application server.

- Using Terraform, an aws ec2 instance is created (along with a VPC, subnet and security group).
- The instance is provisioned to run the PetClinic application
- A curl command ensures that the applciation is running successfully
- The terraform state for this job is uploaded to the S3 Bucket.
- Required parameters: The chosen region, environment, Linux image id, ssh keyname, subnet, name of S3 Bucket, name of project, the instance type, cidr block for the VPC, allowed ip addresses, database endpoint, database master username and database password.
- Outputs: VPC ID, VPC Security Group ID, Default route table for VPC, Internet Gateway ID, Public DNS Name of instance, Instance ID.

##### Create Application AMI #####

Terraform creates an AMI from the existing application instance that has just been built. Every time the application is rebuilt, a new AMI will be created.

- The terraform state from the application Build Job is extracted from the S3 bucket.
- The terraform state recovers the instance id output of the application instance, from which aws_ami_from_instance creates an AMI, named with a timestamp.
- The terraform state for this job is uploaded to the S3 Bucket.
- Required parameters: chosen region, environment, name of S3 Bucket and the name of project.
- Outputs: AMI created

##### Destroy Application Build #####

Once an AMI has been created, the instance running the application is destroyed.

##### Loadbalancer for Application Launch #####

An Elastic Load Balancer is initialised in Terraform with an auto-scaling group to launch the Pet Clinic Application in 3 availability zones.
* The terraform state of the AppAMI job will be found from the S3 bucket, so that the load balancer can read the image output and new ec2 instances provisioned to run the application may be launched.
* The auto-scaling group will determine how many instances (between 1 and 3) need to be built - with a desired count of 2 instances.
* The terraform state from this job is uploaded to the S3 bucket.
* Required variables: region, env, project name, instance type, bucket name, ssh key name, Security Group ID created for the application and the public subnet IDs created by makeVPC.
* Route53 record for PetClinic instances is created.

##### Destroy Networking infrastructure #####

The entire network infrastructure can be destroyed using Jenkins job (dev/prod)destroymakevpcconfig.

##### Destroy Petclinic Loadbalancer #####

The PetClinic loadbalancer (together with any associated infrastructure and PetClinic instances) can be destroyed using Jenkins job (dev/prod)destroylbconfig.

##### Destroy all AMIs #####
This job deletes *all* existing AMIs for the petclinic application launch. If this is carried out, then petclinic cannot be relaunched, it must first be entirely rebuilt and an AMI created again.

##### Timed switching on/off of PetClinic servers #####
The two Jenkins jobs (dev/prod)serversonconfig & (dev/prod)serversoffconfig are responsible for controlling the access the Petclinic. They are run periodically (start at 9am, stop at 6pm). This is achieved by destroying and building the application load balancer.

## Rebuilding
If you have already used the code once and wish to rebuild the infrastructure again from scratch, there are certain steps you must take first.

* Removes the address for the Jenkins instance underneath [jenkins]
* In Jenkins/environments, remove the file ipforjenkins
* Delete .terraform folder from tfstart (other tfstarts will be ignored by gitignore command)





## Error Support

This section contains errors you may encounter and how to solve them.

`fatal: [ec2-3-235-9-41.compute-1.amazonaws.com]: UNREACHABLE! => {"changed": false, "msg": "Failed to connect to the host via ssh: ssh: connect to host ec2-3-235-9-41.compute-1.amazonaws.com port 22: Operation timed out", "unreachable": true}`  
This error may occur if you attempt to create a new jenkins instance without clearing the previous jenkins ec2 address in the hosts file. Change this in Jenkins/environments/(your_environment)/hosts

`Error: error configuring S3 Backend: error validating provider credentials: error calling sts:GetCallerIdentity: ExpiredToken: The security token included in the request is expired
 status code: 403, request id: d6748658-42e2-492f-8689-fe748c9a6a8d`
This error indicates that your aws credentials are out of date and your security token cannot be found. To solve this please log back in to aws and execute the setenv file with your chosen environment as an argument, e.g. ./setenv dev
